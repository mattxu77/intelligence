package com.kunqian.intelligence.dao;

import com.kunqian.intelligence.pojo.News;
import com.kunqian.intelligence.pojo.NewsVO;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface NewsMapper {

    @Select("select count(menu_id) from sys_news where menu_id = #{menuId}")
    public Integer findCountByMenuId(String menuId);

    @Select("select * from sys_news where menu_id = #{menuId} order by orders")
    public List<News> findNewsByMenuId(String menuId);

    @Select(" select id,title,sub_title,author,summery,keyword,source_website,menu_id,orders,pic_path,create_time,context,simple_pic_path from sys_news where menu_id = #{menuId} order by orders")
    public List<News> findNewsByMenuIdPart(String menuId);

    @Select("select * from sys_news where id = #{id}")
    public News findNewsById(Integer id);


    @Insert("insert into sys_news(title,sub_title,author,summery,keyword,source_website,context,menu_id,orders,pic_path, simple_pic_path) " + "values(#{title},#{subTitle},#{author},#{summery},#{keyword},#{sourceWebsite},#{context},#{menuId},#{orders},#{picPath}, #{simplePicPath})")
    public void insertNews(News news);

    @Update("update sys_news set title=#{title},sub_title=#{subTitle},author=#{author},summery=#{summery},keyword=#{keyword}," +
            "source_website=#{sourceWebsite},context=#{context},menu_id=#{menuId},orders=#{orders},pic_path=#{picPath}," +
            "simple_pic_path = #{simplePicPath}" +
            "where id=#{id}")
    public void updateNews(News news);

    @Delete("delete from sys_news where id=#{id}")
    public void deleteNews(Integer id);


    @Select({"<script>select sn.id,IFNULL(sn.title,'') title,IFNULL(sn.sub_title,'') sub_title,\n" +
            "IFNULL(sn.author,'') author,IFNULL(sn.summery,'') summery,\n" +
            "IFNULL(sn.keyword,'') keyword,IFNULL(sn.source_website,'') source_website,\n" +
            "IFNULL((select name from sys_menu where menu_id = sn.menu_id),'')  menu_name,\n" +
            "sn.orders,IFNULL(sn.pic_path,'') pic_path,\n" +
            "DATE_FORMAT(sn.create_time,'%Y-%m-%d %H:%i:%s')  create_time\n" +
            "from sys_news sn where 1=1 <when test='titles!=null'> and INSTR(sn.title,#{titles})>0 </when>\n" +
            "<when test='startTime!=null'>\n" +
            " and sn.create_time &gt;= #{startTime}</when>" +
            "<when test='endTime!=null'> and sn.create_time &lt; #{endTime}</when>  order by sn.orders</script>"})
    public List<Map<String,Object>> findAllNewsTwo(NewsVO news);

    @Delete("delete from sys_news where menu_id = #{menuId}")
    public void deleteByMenuId(String menuId);

}
