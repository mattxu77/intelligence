package com.kunqian.intelligence.dao;

import com.kunqian.intelligence.pojo.Menu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface MenuMapper {

    @Select(" select * from sys_menu where parent_id = 0 order by order_num ")
    public List<Menu> getAllMenu();

    @Select("select menu_id,parent_id,name,ifNull('',url) url,order_num,eng_name,comment from sys_menu")
    public List<Map<String, Object>> getAllMenuMap();

    @Insert("insert into sys_menu(parent_id, name, url, order_num,eng_name,comment) values(#{parentId},#{name},#{url},#{orderNum},#{engName},#{comment})")
    public void insertMenu(Menu menu);

    @Update("update sys_menu set name=#{name},order_num=#{orderNum},eng_name=#{engName},comment=#{comment} where menu_id=#{menuId}")
    public void updateMenu(Menu menu);

    @Delete(" delete from sys_menu where menu_id = #{menuId}")
    public void deleteMenu(String menuId);

    @Select("select * from sys_menu where menu_id = #{menuId}")
    public Menu findMenuById(String menuId);
}
