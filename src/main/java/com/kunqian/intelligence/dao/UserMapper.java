package com.kunqian.intelligence.dao;

import com.kunqian.intelligence.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper {

    @Select("select * from user where username = #{username}")
    User findByUsername(String username);

    @Update("update user set password = #{newPassword} where username = #{username}")
    void editPassword(@Param("username") String username, @Param("newPassword") String newPassword);

    @Insert("insert into user values(#{id}, #{username}, #{password}, #{phone}, 1)")
    @SelectKey(statement = "SELECT @@identity", keyProperty = "id", before = false, resultType = Integer.class)
    Integer add(User user);

    @Update("update user set phone = #{phone}, state = #{state} where id = #{id}")
    void update(User user);

    @Select("select * from user where state = 1")
    List<User> findAll();

    @Delete("delete from user where username = #{username}")
    void delete(String username);
}
