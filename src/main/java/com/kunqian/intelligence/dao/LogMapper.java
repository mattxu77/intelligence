/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.kunqian.intelligence.dao;


import com.kunqian.intelligence.pojo.Log;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 系统日志
 *
 * @author jianwei
 */
@Mapper
public interface LogMapper {

    @Select("select * from sys_log order by create_date desc")
    List<Log> findAll();

    @Select({"<script>select * from sys_log where 1=1 " +
            "<when test='key!=null'> and INSTR(CONCAT(username,operation),#{key}) > 0 </when>" +
            "order by create_date desc</script>"})
    List<Log> findPageByKey(@Param("key") String key);

    @Insert("insert into sys_log values(#{id},#{username},#{operation},#{method},#{params},#{time},#{ip},#{createDate})")
    void insert(Log sysLog);


}
