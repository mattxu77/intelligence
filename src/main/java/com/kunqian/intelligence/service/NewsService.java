package com.kunqian.intelligence.service;

import com.kunqian.intelligence.pojo.News;
import com.kunqian.intelligence.pojo.NewsVO;
import com.kunqian.intelligence.pojo.PageResult;

import java.text.ParseException;
import java.util.List;

public interface NewsService {

    List<News> findNewsByMenuId(String menuId);

    PageResult getPageResult(String menuId, Integer pageNo, Integer pageSize);

    News findNewsById(Integer id);

    void insertNews(News news);

    void updateNews(News news);

    void deleteNews(Integer id);

    PageResult findAllNews(NewsVO news) throws ParseException;

    void deleteByMenuId(String menuId);

    Integer findCountByMenuId(String menuId);
}
