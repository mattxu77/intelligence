/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.kunqian.intelligence.service;


import com.kunqian.intelligence.pojo.Log;
import com.kunqian.intelligence.pojo.PageResult;


/**
 * 系统日志
 *
 * @author jianwei
 */
public interface LogService {

    /**
     * 分页查询
     *
     * @param page     当前页
     * @param pageSize 每页条数
     * @return
     */
    PageResult<Log> findPage(Integer page, Integer pageSize);

    /**
     * 模糊查询
     *
     * @param page
     * @param rows
     * @param key
     * @return
     */
    PageResult search(Integer page, Integer rows, String key);

    /**
     * 插入数据
     *
     * @param sysLog
     */
    void insert(Log sysLog);


}
