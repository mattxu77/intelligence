package com.kunqian.intelligence.service;

import com.kunqian.intelligence.pojo.User;

import java.util.List;


public interface UserService {

    /**
     * 查询所有
     *
     * @return
     */
    List<User> findAll();

    /**
     * 根据用户名查询用户
     *
     * @param username
     * @return
     */
    User findByUsername(String username);

    /**
     * 修改密码
     *
     * @param username
     * @param newPassword
     */
    void editPassword(String username, String newPassword);

    /**
     * 新增用户
     *
     * @param user
     */
    User add(User user);

    /**
     * 修改用户信息
     *
     * @param user
     */
    void update(User user);


    /**
     * 删除用户
     *
     * @param username
     */
    void delete(String username);
}
