package com.kunqian.intelligence.service.impl;

import com.kunqian.intelligence.dao.MenuMapper;
import com.kunqian.intelligence.pojo.Menu;
import com.kunqian.intelligence.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;

    public List<Menu> findAllMenu() {

        List<Menu> menuList = menuMapper.getAllMenu();


        /*List<Map<String,Object>> listMap = menuMapper.getAllMenuMap();

        List<Map<String,Object>> fList = new ArrayList<>();
        List<Map<String,Object>> sList = new ArrayList<>();
        for(Map list:listMap) {
            if (Integer.valueOf(list.get("parent_id").toString()) == 0)
                fList.add(list);
            else
                sList.add(list);
        }
        for(Map fl:fList){
            List<Map<String,Object>> sonList = new ArrayList<>();
            for(Map sl:sList){
                if(Integer.valueOf(fl.get("menu_id").toString()) == Integer.valueOf(sl.get("parent_id").toString()))
                    sonList.add(sl);
            }
            fl.put("sList",sonList);
        }*/


        //return fList;
        return menuList;
    }

    public void insertMenu(Menu menu) {
        menu.setParentId(0l);
        menuMapper.insertMenu(menu);
    }

    public void updateMenu(Menu menu) {
        menuMapper.updateMenu(menu);
    }

    public void deleteMenu(String menuId) {
        menuMapper.deleteMenu(menuId);
    }

    public List<Menu> findMenuSelection() {
        return menuMapper.getAllMenu();
    }

    public Menu findMenuById(String menuId){
        return menuMapper.findMenuById(menuId);
    }
}
