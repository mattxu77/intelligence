package com.kunqian.intelligence.service.impl;

import com.kunqian.intelligence.dao.UserMapper;
import com.kunqian.intelligence.pojo.User;
import com.kunqian.intelligence.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }

    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }

    @Override
    public void editPassword(String username, String newPassword) {
        userMapper.editPassword(username, newPassword);
    }

    @Override
    public User add(User user) {
        Integer id = userMapper.add(user);
        return user;
    }

    @Override
    public void update(User user) {
        userMapper.update(user);
    }

    @Override
    public void delete(String username) {
        userMapper.delete(username);
    }
}
