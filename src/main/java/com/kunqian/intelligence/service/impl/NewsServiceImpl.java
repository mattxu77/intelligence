package com.kunqian.intelligence.service.impl;

import com.github.pagehelper.PageHelper;
import com.kunqian.intelligence.dao.NewsMapper;
import com.kunqian.intelligence.pojo.News;
import com.kunqian.intelligence.pojo.NewsVO;
import com.kunqian.intelligence.pojo.PageResult;
import com.kunqian.intelligence.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsMapper newsMapper;

    @Value("${spring.http.multipart.location}")
    private String classPath;

    public List<News> findNewsByMenuId(String menuId) {

        Integer count = newsMapper.findCountByMenuId(menuId);
        if (count == 1)
            return newsMapper.findNewsByMenuId(menuId);
        else
            return newsMapper.findNewsByMenuIdPart(menuId);
    }

    public PageResult getPageResult(String menuId, Integer pageNo, Integer pageSize) {

        PageHelper.startPage(pageNo, pageSize);
        List<News> list = newsMapper.findNewsByMenuIdPart(menuId);
        return new PageResult<>(list);
    }

    public News findNewsById(Integer id) {
        return newsMapper.findNewsById(id);
    }


    @Override
    @Transactional
    public void insertNews(News news) {
        newsMapper.insertNews(news);
    }

    @Override
    @Transactional
    public void updateNews(News news) {
        //后删除图片
        News newss = newsMapper.findNewsById(news.getId());
        newsMapper.updateNews(news);

        if (newss.getPicPath() != null && !"".equals(newss.getPicPath()))
            fileDelete(newss.getPicPath());
        if (newss.getSimplePicPath() != null && !"".equals(newss.getSimplePicPath()))
            fileDelete(newss.getSimplePicPath());
    }

    @Override
    @Transactional
    public void deleteNews(Integer id) {
        //后删除图片
        News news = newsMapper.findNewsById(id);
        newsMapper.deleteNews(id);

        if (news.getPicPath() != null && !"".equals(news.getPicPath()))
            fileDelete(news.getPicPath());
        if (news.getSimplePicPath() != null && !"".equals(news.getSimplePicPath()))
            fileDelete(news.getSimplePicPath());
    }

    public void fileDelete(String filePath) {
        String paths[] = filePath.split(",");
        for (String path : paths) {
            File file = new File(classPath.concat(path));
            file.delete();
        }
    }

    public PageResult findAllNews(NewsVO news) throws ParseException {
        SimpleDateFormat sim = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        if(news.getStartTime() != null)
            news.setStartTime(sim.parse(sim.format(news.getStartTime())));
        if(news.getEndTime() != null) {
            cal.setTime(sim.parse(sim.format(news.getEndTime())));
            cal.add(Calendar.DATE, 1);
            news.setEndTime(cal.getTime());
            System.out.println(cal.getTime());
        }
        PageHelper.startPage(news.getPageNo(), news.getPageSize());
        //List<Map<String,Object>> list = newsMapper.findAllNews(news.getNewz());
        List<Map<String,Object>> listz = newsMapper.findAllNewsTwo(news);
        return new PageResult<>(listz);
    }

    public void deleteByMenuId(String menuId) {
        newsMapper.deleteByMenuId(menuId);
    }

    public Integer findCountByMenuId(String menuId) {
        return newsMapper.findCountByMenuId(menuId);
    }

}
