/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.kunqian.intelligence.service.impl;

import com.github.pagehelper.PageHelper;
import com.kunqian.intelligence.dao.LogMapper;
import com.kunqian.intelligence.pojo.Log;
import com.kunqian.intelligence.pojo.PageResult;
import com.kunqian.intelligence.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("sysLogService")
public class LogServiceImpl implements LogService {


    @Autowired
    private LogMapper sysLogMapper;

    @Override
    public PageResult<Log> findPage(Integer page, Integer rows) {
        PageHelper.startPage(page, rows);
        List<Log> list = sysLogMapper.findAll();
        return new PageResult<>(list);
    }

    @Override
    public PageResult search(Integer page, Integer rows, String key) {
        PageHelper.startPage(page, rows);
        List<Log> list = sysLogMapper.findPageByKey(key);
        return new PageResult<>(list);
    }

    @Override
    public void insert(Log sysLog) {
        sysLogMapper.insert(sysLog);
    }
}
