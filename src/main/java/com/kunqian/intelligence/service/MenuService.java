package com.kunqian.intelligence.service;

import com.kunqian.intelligence.pojo.Menu;

import java.util.List;

public interface MenuService {

    List<Menu> findAllMenu();

    void insertMenu(Menu menu);

    void updateMenu(Menu menu);

    void deleteMenu(String menuId);

    List<Menu> findMenuSelection();

    Menu findMenuById(String menuId);
}
