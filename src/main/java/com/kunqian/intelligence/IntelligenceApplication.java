package com.kunqian.intelligence;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.kunqian.intelligence.dao"})

public class IntelligenceApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(IntelligenceApplication.class, args);
    }

//    @Autowired
//    private User user;

    @Override
    public void run(String... strings) throws Exception {
//        System.out.println(1);
    }


}

