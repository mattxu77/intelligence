package com.kunqian.intelligence.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 新闻实体类
 *
 * @author jw
 */
public class News implements Serializable {

    /**
     * id
     */
    private Integer id;
    /**
     * 标题
     */
    private String title;
    /**
     * 副标题
     */
    private String subTitle;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 作者姓名
     */
    private String author;
    /**
     * 简介
     */
    private String summery;
    /**
     * 关键字
     */
    private String keyword;
    /**
     * 来源网站
     */
    private String sourceWebsite;
    /**
     * 内容
     */
    private String context;
    /**
     * 分类
     */
    private Integer menuId;

    /**
     * 图片路径
     */
    private String picPath;

    /**
     * 略缩图路径
     */
    private String simplePicPath;

    /**
     * 创建时间
     */
    private Date createTime;

    public News() {
    }

    public News(Integer id, String title, String subTitle, Integer orders, String author, String summery, String keyword, String sourceWebsite, String context, Integer menuId, String picPath, String simplePicPath, Date createTime) {
        this.id = id;
        this.title = title;
        this.subTitle = subTitle;
        this.orders = orders;
        this.author = author;
        this.summery = summery;
        this.keyword = keyword;
        this.sourceWebsite = sourceWebsite;
        this.context = context;
        this.menuId = menuId;
        this.picPath = picPath;
        this.simplePicPath = simplePicPath;
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSummery() {
        return summery;
    }

    public void setSummery(String summery) {
        this.summery = summery;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSourceWebsite() {
        return sourceWebsite;
    }

    public void setSourceWebsite(String sourceWebsite) {
        this.sourceWebsite = sourceWebsite;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public String getSimplePicPath() {
        return simplePicPath;
    }

    public void setSimplePicPath(String simplePicPath) {
        this.simplePicPath = simplePicPath;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", orders=" + orders +
                ", author='" + author + '\'' +
                ", summery='" + summery + '\'' +
                ", keyword='" + keyword + '\'' +
                ", sourceWebsite='" + sourceWebsite + '\'' +
                ", context='" + context + '\'' +
                ", menuId=" + menuId +
                ", picPath='" + picPath + '\'' +
                ", simplePicPath='" + simplePicPath + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
