package com.kunqian.intelligence.pojo;

import com.github.pagehelper.PageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * 分页信息
 *
 * @author jianwei
 */
public class PageResult<T> implements Serializable {

    /**
     * 当前页数
     */
    private int page;
    /**
     * 总页数
     */
    private int total;
    /**
     * 总记录数
     */
    private long records;
    /**
     * 每行显示的内容
     */
    private List<?> rows;
    /**
     * 用户自定义数据
     */
    private Object userdata;


    private PageInfo<T> pageInfo;

    public PageResult(List<T> list) {
        this.pageInfo = new PageInfo<>(list);

        this.page = pageInfo.getPageNum();
        this.total = pageInfo.getPages();
        this.records = pageInfo.getTotal();
        this.rows = list;
    }


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public long getRecords() {
        return records;
    }

    public void setRecords(long records) {
        this.records = records;
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }

    public Object getUserdata() {
        return userdata;
    }

    public void setUserdata(Object userdata) {
        this.userdata = userdata;
    }
}
