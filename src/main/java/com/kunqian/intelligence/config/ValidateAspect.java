package com.kunqian.intelligence.config;

import com.kunqian.intelligence.pojo.User;
import com.kunqian.intelligence.util.HttpContextUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ValidateAspect {

    @Pointcut("@annotation(com.kunqian.intelligence.config.Validate)")
    public void addAdvice(){};

    @Before("addAdvice()")
    public void validate(JoinPoint point){

        User userz = (User)HttpContextUtils.getHttpServletRequest().getServletContext().getAttribute("user");
        User user = (User)HttpContextUtils.getHttpServletRequest().getSession().getAttribute("user");
        if(user == null || (user != null && (user.getUsername() ==null || "".equals(user.getUsername())))){
            throw new SecurityException("用户没有登陆，无权访问！");
        }
    }
}
