package com.kunqian.intelligence.controller;

import com.alibaba.druid.sql.ast.statement.SQLIfStatement;
import com.kunqian.intelligence.config.SysLog;
import com.kunqian.intelligence.config.Validate;
import com.kunqian.intelligence.pojo.User;
import com.kunqian.intelligence.service.UserService;
import com.kunqian.intelligence.util.HttpContextUtils;
import com.kunqian.intelligence.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
@Api(description = "用户模块", value = "/user")
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("查询所有用户")
    @GetMapping("/findAll")
    @Validate
    public R findAll(HttpServletRequest request) {
        User us = (User)request.getServletContext().getAttribute("user");
        User user = (User)request.getSession().getAttribute("user");
        System.out.println("getSession:"+request.getSession().getId());
        if(user == null)
            return R.error(300,"用户失效，重新登陆");
        else {
            List<User> users = new ArrayList<>();
            if ("admin".equals(user.getUsername()))
                users = userService.findAll();
            else
                users.add(userService.findByUsername(user.getUsername()));
            return R.ok().put("users", users);
        }
    }

    @ApiOperation("根据用户名查询用户")
    @GetMapping("{username}")
    @Validate
    public R findByUserName(@PathVariable String username){
        User user = userService.findByUsername(username);
        return R.ok().put("user",user);
    }

    @SysLog("修改密码")
    @ApiOperation("修改密码")
    @Validate
    @PutMapping("{username}/{password}/{newPassword}/{currentUser}")
    public R editPassword(@ApiParam("用户名") @PathVariable String username, @ApiParam("旧密码")
        @PathVariable String password, @ApiParam("新密码") @PathVariable String newPassword,@ApiParam("当前用户") @PathVariable String currentUser){
        User user = userService.findByUsername(username);
        if(!"admin".equals(currentUser))
             if (!password.equals(user.getPassword()))
                return R.error("原密码错误");
        userService.editPassword(username,newPassword);
        return R.ok();
    }

    @SysLog("新增用户")
    @ApiOperation("新增用户")
    @PostMapping()
    @Validate
    public R add(@RequestBody User user){
        User user1 = userService.findByUsername(user.getUsername());
        if (user1 != null)
            return R.error("用户名已存在,换一个试试");
        User returnUser = userService.add(user);
        //user.setId(id);
        return R.ok().put("user",returnUser);
    }

    @SysLog("更新用户")
    @ApiOperation("根据id更新用户")
    @PutMapping()
    @Validate
    public R update(@RequestBody User user){
        userService.update(user);
        return R.ok();
    }

    @SysLog("删除用户")
    @ApiOperation("删除用户")
    @DeleteMapping("{username}")
    @Validate
    public R delete(@PathVariable String username) {
        if ("admin".equals(username))
            return R.error("系统管理员不可以被删除");
        userService.delete(username);
        return R.ok();
    }
}
