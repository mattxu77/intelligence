package com.kunqian.intelligence.controller;

import com.kunqian.intelligence.config.SysLog;
import com.kunqian.intelligence.config.Validate;
import com.kunqian.intelligence.pojo.Menu;
import com.kunqian.intelligence.service.MenuService;
import com.kunqian.intelligence.service.NewsService;
import com.kunqian.intelligence.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("menu")
@Api(description = "菜单模块", value = "/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @Autowired
    private NewsService newsService;

    @ApiOperation("查询所有菜单")
    @GetMapping()
    public R findAllMenu() {
        //List<Map<String,Object>> menuList = menuService.findAllMenu();
        List<Menu> menuList = menuService.findAllMenu();
        return R.ok().put("menuList", menuList);
    }

    @SysLog("新增菜单")
    @ApiOperation("新增菜单")
    @PostMapping()
    @Validate
    public R addMenu(@RequestBody Menu menu) {

        menuService.insertMenu(menu);
        List<Menu> menuList = menuService.findAllMenu();
        return R.ok().put("menuList", menuList);
    }

    @SysLog("更新菜单")
    @ApiOperation("更新菜单")
    @PutMapping()
    @Validate
    public R updateMenu(@RequestBody Menu menu) {

        menuService.updateMenu(menu);
        List<Menu> menuList = menuService.findAllMenu();


        return R.ok().put("menuList", menuList);
    }

    @SysLog("删除菜单")
    @ApiOperation("删除菜单")
    @DeleteMapping("{menuId}")
    @Validate
    public R deleteMenu(@PathVariable String menuId) {

        //删除目录下的新闻
        // newsService.deleteByMenuId(menuId);
        //删除目录
        menuService.deleteMenu(menuId);

        return R.ok();
    }


    @SysLog("选择菜单")
    @ApiOperation("选择菜单")
    @GetMapping("/menuSelection")
    @Validate
    public R menuSelection() {

        List<Menu> menuList = menuService.findMenuSelection();
       /* Map<String,Object> menu = new HashMap<>();
        menu.put("menuId",0l);
        menu.put("name","一级菜单");
        menu.put("sList",menuList);*/

        return R.ok().put("selectMenu", menuList);
    }
}
