package com.kunqian.intelligence.controller;

import com.kunqian.intelligence.config.SysLog;
import com.kunqian.intelligence.pojo.User;
import com.kunqian.intelligence.service.UserService;
import com.kunqian.intelligence.util.HttpContextUtils;
import com.kunqian.intelligence.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Api(description = "登录模块")
@RestController
public class LoginController {

    @Autowired
    private UserService userService;

    @ApiOperation("登录接口")
    @SysLog("登录")
    @PostMapping("/login")
    public R login(String username, String password, HttpServletRequest request, HttpServletResponse response) {
        User user = userService.findByUsername(username);
        if (user == null)
            return R.error("用户名不存在");
        if (!password.equals(user.getPassword()))
            return R.error("密码错误");

        request.getServletContext().setAttribute("user",user);

        //存入session
        HttpSession session = request.getSession();
        request.getSession().setAttribute("user",user);

        //存入cookie
        session.setAttribute("user", user);
        String jsessionid = session.getId();
        System.out.println("jsessionid = " + jsessionid);
        Cookie cookie = new Cookie("jsessionid",jsessionid);
        cookie.setMaxAge(30 * 60);
        response.addCookie(cookie);


        return R.ok();
    }

    @ApiOperation("获取当前用户")
    @GetMapping("/getUser")
    public R getUser(HttpServletRequest request){
        User us = (User)request.getServletContext().getAttribute("user");
        User user = (User)request.getSession().getAttribute("user");
        if(user == null)
            return R.error(300,"用户失效，重新登陆");
        else
            return R.ok().put("username",user.getUsername());
    }

    @ApiOperation("登出当前用户")
    @GetMapping("/logOut")
    public R logOut(HttpServletRequest request){
        request.getServletContext().removeAttribute("user");
        request.getSession().removeAttribute("user");
        return R.ok();
    }


//    @RequestMapping(value = "/loginPage", method = RequestMethod.GET)
//    public R login() {
//        return R.error(886,"未登录");
//    }
//
//    @RequestMapping("/index")
//    public R root() {
//        return R.error(200,"请跳到首页");
//    }
//
//    @GetMapping("getUser")
//    public R getUser() { //为了session从获取用户信息,可以配置如下
//        User user = new User();
//        SecurityContext ctx = SecurityContextHolder.getContext();
//        Authentication auth = ctx.getAuthentication();
//        if (auth.getPrincipal() instanceof UserDetails) user = (User) auth.getPrincipal();
//        return R.ok().put("user",user);
//    }
//
//    public HttpServletRequest getRequest() {
//        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
//    }
}
