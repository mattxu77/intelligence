package com.kunqian.intelligence.controller;

import com.kunqian.intelligence.config.SysLog;
import com.kunqian.intelligence.config.Validate;
import com.kunqian.intelligence.util.FileUtils;
import com.kunqian.intelligence.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("upload")
@Api(description = "上传文件模块", value = "/upload")
public class uploadController {

    @Value("${spring.http.multipart.location}")
    private String path;

    @Value("${image.path}")
    private String imagePath;

    /**
     * 上传图片, paths 是上传的文件在服务器中绝对路径的集合
     *
     * @param request
     * @return
     */
    @SysLog("上传图片")
    @PostMapping(headers = "content-type=multipart/form-data", value = "fileUpload")
    @ApiOperation("上传图片")
    public R fileUpload(HttpServletRequest request) {

        List<String> paths = FileUtils.upload(request, path);
        if (paths == null || paths.size() < 1) {
            return R.error("上传图片失败");
        }
        return R.ok().put("paths", paths);

    }




    @SysLog("上传略缩图")
    @PostMapping(headers = "content-type=multipart/form-data", value = "simpleFileUpload")
    @ApiOperation("上传略缩图")
    public R simpleFileUpload(MultipartFile file) {

        String simplePicPaths = FileUtils.upload(file, path,imagePath);
        if ("empty".equals(simplePicPaths))
            return R.error("文件为空");
        if ("".equals(simplePicPaths)) {
            return R.error("上传图片失败");
        }
        return R.ok().put("simplePicPaths", simplePicPaths);

    }


    @SysLog("获取文件存储路径")
    @ApiOperation("获取文件存储路径")
    @GetMapping
    @Validate
    public R getPath() {
        return R.ok().put("path",path);
    }


    /**
     * 多文件上传
     * @param uploadFile
     * @return
     * @throws IllegalStateException
     */
    @Validate
    @PostMapping(/*headers = "content-type=multipart/form-data",*/value = "/multiUploadPic")
    public Object multiUpload(@RequestParam("uploadFile") MultipartFile[] uploadFile)
            throws Exception {

        Map<String,Object> map = new HashMap<>();
        //路径列表
        List<String> paths = new ArrayList<>();
        try {
            //保存文件的目录
            if(null != uploadFile && uploadFile.length > 0){
                //遍历并保存文件
                for(MultipartFile file : uploadFile){
                    // 获取图片的文件名
                    String fileName = file.getOriginalFilename();
                    // 获取图片的扩展名
                    String extensionName = fileName.substring(fileName.lastIndexOf(".") + 1);
                    // 新的图片文件名 = 获取时间戳+"."图片扩展名
                    String newFileName = String.valueOf(System.currentTimeMillis()) + "." + extensionName;
                    file.transferTo(new File(path + newFileName));
                    paths.add(imagePath + newFileName);
                }
            }
            map.put("error",0);
            map.put("url",paths.get(0));
        }catch (SecurityException e){
            map.put("error",300);
            map.put("message","用户没有登陆，无权访问！");
        }catch (Exception e){
            map.put("error",1);
            map.put("message","图片地址错误");
        }

        return map;
    }

    /*private final ResourceLoader resourceLoader;

    @Autowired
    public uploadController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }*/


//     *
//     *
//     * @param file 要上传的文件
//     * @return

    /*@RequestMapping("up")
    public R upload(@RequestParam("fileName") MultipartFile file){

        // 要上传的目标文件存放路径
        String localPath = path;

        if (!FileUtils.upload(file, localPath, file.getOriginalFilename()))
            return R.error("上传失败");
        // 上传成功
        return R.ok().put("fileName", file.getOriginalFilename());
    }*/
    /**
     * 显示单张图片
     * @return
     */
    /*@RequestMapping("show")
    public ResponseEntity showPhotos(String fileName){

        try {
            // 由于是读取本机的文件，file是一定要加上的， path是在application配置文件中的路径
            return ResponseEntity.ok(resourceLoader.getResource("file:" + path + fileName));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
*/


}


