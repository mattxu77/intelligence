/**
 * Copyright 2018 人人开源 http://www.renren.io
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.kunqian.intelligence.controller;

import com.kunqian.intelligence.config.SysLog;
import com.kunqian.intelligence.config.Validate;
import com.kunqian.intelligence.pojo.Log;
import com.kunqian.intelligence.pojo.NewsVO;
import com.kunqian.intelligence.pojo.PageResult;
import com.kunqian.intelligence.service.LogService;
import com.kunqian.intelligence.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * 系统日志
 *
 * @author jianwei
 */
@RestController
@RequestMapping("/log")
@Api(description = "日志模块", value = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    /**
     * 列表
     */
    @SysLog("查询日志")
    @ApiOperation("分页模糊查询所有日志")
    @PostMapping()
    @Validate
    public R list(@RequestBody NewsVO vo) {
        if (vo.getPageNo() < 1)
            vo.setPageNo(1);
        PageResult logs = logService.search(vo.getPageNo(), vo.getPageSize(), vo.getTitles());
        return R.ok().put("logs", logs);
    }

}
