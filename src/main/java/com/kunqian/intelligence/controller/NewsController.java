package com.kunqian.intelligence.controller;

import com.kunqian.intelligence.config.SysLog;
import com.kunqian.intelligence.config.Validate;
import com.kunqian.intelligence.pojo.Menu;
import com.kunqian.intelligence.pojo.News;
import com.kunqian.intelligence.pojo.NewsVO;
import com.kunqian.intelligence.pojo.PageResult;
import com.kunqian.intelligence.service.MenuService;
import com.kunqian.intelligence.service.NewsService;
import com.kunqian.intelligence.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("news")
@Api(description = "新闻模块", value = "/news")
public class NewsController {

    @Autowired
    private NewsService newsService;
    @Autowired
    private MenuService menuService;

    /**
     * 单个目录新闻
     * @param menuId
     * @return
     */
    @ApiOperation("根据菜单查询该菜单下的所有新闻")
    @GetMapping("/menuId/{menuId}")
    public R findNewsByMenuId(@PathVariable String menuId){
        Menu menu = menuService.findMenuById(menuId);
        List<News> list = newsService.findNewsByMenuId(menuId);
        return R.ok().put("newsList",list).put("menu",menu);
    }

    /**
     * 目录返回分页新闻结果
     * @param menuId
     * @param pageSize
     * @param pageNo
     * @return
     */
    @ApiOperation("根据菜单分页查询该菜单下的所有新闻")
    @GetMapping("{menuId}/{pageNo}/{pageSize}")
    public R findNewsByPage(@PathVariable String menuId,@PathVariable Integer pageNo,@PathVariable Integer pageSize){

        PageResult page = newsService.getPageResult(menuId,pageNo,pageSize);
        return R.ok().put("page",page);
    }

    @ApiOperation("根据id查询新闻")
    @GetMapping("{id}")
    public R findNewsById(@PathVariable Integer id){

        News news = newsService.findNewsById(id);
        return R.ok().put("news",news);
    }


    @SysLog("新增新闻")
    @ApiOperation("新增新闻")
    @PostMapping()
    @Validate
    public R addNews(@RequestBody News news){
        newsService.insertNews(news);
        return R.ok();
    }

    @SysLog("更新新闻")
    @ApiOperation("更新新闻")
    @PutMapping()
    @Validate
    public R updateNews(@RequestBody News news){
        newsService.updateNews(news);
        return R.ok();
    }

    @SysLog("删除新闻")
    @ApiOperation("删除新闻")
    @DeleteMapping("{id}")
    @Validate
    public R deleteNews(@PathVariable Integer id){
        newsService.deleteNews(id);
        return R.ok();
    }

    @ApiOperation("查询全部新闻")
    @CrossOrigin
    @PostMapping("/findAll")
    @Validate
    public R findAllNews(@RequestBody NewsVO news) throws ParseException {
        PageResult page = newsService.findAllNews(news);
        return R.ok().put("page",page);
    }

    @ApiOperation("查询目录下的新闻数量")
    @PostMapping("/newsCount")
    @Validate
    public R findCountByMenuId(String menuId){
        Integer count = newsService.findCountByMenuId( menuId);
        return R.ok().put("count",count);
    }

}
