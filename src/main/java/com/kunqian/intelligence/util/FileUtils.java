package com.kunqian.intelligence.util;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    /**
     *
     * @param file 文件
     * @param path 文件存放路径
     * @param fileName 源文件名
     * @return
     */
    /*public static boolean upload(MultipartFile file, String path, String fileName){

        // 生成新的文件名
        String realPath = path + "/" + getFileName(fileName);

        //使用原文件名
        //String realPath = path + "/" + fileName;

        File dest = new File(realPath);

        //判断文件父目录是否存在
        if(!dest.getParentFile().exists()){
            dest.getParentFile().mkdir();
        }


        try {
            //保存文件
            file.transferTo(dest);
            return true;
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

    }


    *//**
     * 生成新的文件名
     * @param fileOriginName 源文件名
     * @return
     *//*
    public static String getFileName(String fileOriginName){
        return UUID.randomUUID().toString().replaceAll("\\-", "")  + getSuffix(fileOriginName);
    }

    *//**
     * 获取文件后缀
     * @param fileName
     * @return
     *//*
    public static String getSuffix(String fileName){
        return fileName.substring(fileName.lastIndexOf("."));
    }*/

    /**
     * 上传单个图片
     *
     * @param file
     * @param uploadPath
     * @return
     */
    public static String upload(MultipartFile file, String uploadPath,String imagePath) {
        if (file.isEmpty())
            return "empty";
        try {
            byte[] bytes = file.getBytes();
            // 获取图片的文件名
            String fileName = file.getOriginalFilename();
            // 获取图片的扩展名
            String extensionName = fileName.substring(fileName.lastIndexOf(".") + 1);
            // 新的图片文件名 = 获取时间戳+"."图片扩展名
            String newFileName = String.valueOf(System.currentTimeMillis()) + "." + extensionName;
            //图片路径
            Path path = Paths.get(uploadPath + newFileName);
            //如果没有files文件夹，则创建
            if (!Files.isWritable(path)) {
                Files.createDirectories(Paths.get(uploadPath));
            }
            //文件写入指定路径
            Files.write(path, bytes);
            //文件名保存起来
            return imagePath + newFileName + "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    /**
     * 批量上传图片
     *
     * @param request    请求域
     * @param uploadPath 要存放图片位置
     * @return 包含文件的绝对路径的集合
     */
    public static List<String> upload(HttpServletRequest request, String uploadPath) {
        MultipartHttpServletRequest params = ((MultipartHttpServletRequest) request);
        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("file");
        System.out.println("files" + files);
        //完整路径
        List<String> paths = new ArrayList<>();
        //接收前端传过来的字段
        //String name = params.getParameter("name");
        MultipartFile file = null;
        //先设置一个数组来装file路径
//        List<Path> imgs = new ArrayList<>();

        for (int i = 0; i < files.size(); ++i) {
            file = files.get(i);
            if (!file.isEmpty()) {
                try {
                    byte[] bytes = file.getBytes();
                    // 获取图片的文件名
                    String fileName = file.getOriginalFilename();
                    // 获取图片的扩展名
                    String extensionName = fileName.substring(fileName.lastIndexOf(".") + 1);
                    // 新的图片文件名 = 获取时间戳+"."图片扩展名
                    String newFileName = String.valueOf(System.currentTimeMillis()) + "." + extensionName;
                    //图片路径
                    Path path = Paths.get(uploadPath + newFileName);
//                    System.out.println("lj: " + path);
//                    imgs.add(path);
                    //如果没有files文件夹，则创建
                    if (!Files.isWritable(path)) {
                        Files.createDirectories(Paths.get(uploadPath));
                    }
                    //文件写入指定路径
                    Files.write(path, bytes);
                    //文件名保存起来
                    paths.add(path + "");
                } catch (Exception e) {
                    e.printStackTrace();
                    paths.clear();
                    return paths;
                }
            } else {
                System.err.println(i + " failed, because the file was empty.");
                paths.clear();
                return paths;
            }
        }
        return paths;
    }


}
